package com.test.webflux.trx.mongodb.service.impl;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.dao.User;
import com.test.webflux.trx.mongodb.entity.dao.UserContact;
import com.test.webflux.trx.mongodb.entity.dao.UserRole;
import com.test.webflux.trx.mongodb.outbound.MessageOutboundService;
import com.test.webflux.trx.mongodb.service.AccountService;
import com.test.webflux.trx.mongodb.service.UserContactService;
import com.test.webflux.trx.mongodb.service.UserRoleService;
import com.test.webflux.trx.mongodb.service.UserService;
import com.test.webflux.trx.mongodb.utils.helper.RetryHelper;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@Service
public class AccountServiceImpl implements AccountService {

  @Autowired
  private UserService userService;

  @Autowired
  private UserContactService userContactService;

  @Autowired
  private UserRoleService userRoleService;

  @Autowired
  private MessageOutboundService messageOutboundService;

  @Override
  public Mono<Boolean> successWithoutTrx(MandatoryRequest mandatoryRequest, boolean usingCache) {
    return this.doAction(mandatoryRequest, usingCache)
        .flatMap(aBoolean -> messageOutboundService.sendMessage(mandatoryRequest, true));
  }

  @Override
  @Transactional
  public Mono<Boolean> successWithTrx(MandatoryRequest mandatoryRequest, boolean usingCache) {
    return this.doAction(mandatoryRequest, usingCache)
        .flatMap(aBoolean -> messageOutboundService.sendMessage(mandatoryRequest, true));
  }

  @Override
  public Mono<Boolean> errorWithoutTrx(MandatoryRequest mandatoryRequest, boolean usingCache) {
    return this.doAction(mandatoryRequest, usingCache)
        .flatMap(aBoolean -> messageOutboundService.sendMessage(mandatoryRequest, false))
        .retryWhen(RetryHelper.retrySpec());
  }

  @Override
  @Transactional
  public Mono<Boolean> errorWithTrx(MandatoryRequest mandatoryRequest, boolean usingCache) {
    return this.doAction(mandatoryRequest, usingCache)
        .flatMap(aBoolean -> messageOutboundService.sendMessage(mandatoryRequest, false))
        .retryWhen(RetryHelper.retrySpec());
  }

  private Mono<Boolean> doAction(MandatoryRequest mandatoryRequest, boolean usingCache) {
    return userService.create(mandatoryRequest, this.constructUser(), usingCache)
        .flatMap(user -> userContactService
            .create(mandatoryRequest, this.constructUserContact(user.getId(), user.getHashId()),
                usingCache)
            .flatMap(userContact -> userRoleService
                .create(mandatoryRequest, this.constructUserRole(user.getId(), user.getHashId()),
                    usingCache)))
        .thenReturn(Boolean.TRUE);
  }

  private User constructUser() {
    return User.builder()
        .name("user-" + UUID.randomUUID().toString())
        .build();
  }

  private UserRole constructUserRole(String userId, String userHashId) {
    return UserRole.builder()
        .roleName("role-" + UUID.randomUUID().toString())
        .userId(userId)
        .userHashId(userHashId)
        .build();
  }

  private UserContact constructUserContact(String userId, String userHashId) {
    return UserContact.builder()
        .email("email-" + UUID.randomUUID().toString())
        .phone("phone-" + UUID.randomUUID().toString())
        .userId(userId)
        .userHashId(userHashId)
        .build();
  }
}
