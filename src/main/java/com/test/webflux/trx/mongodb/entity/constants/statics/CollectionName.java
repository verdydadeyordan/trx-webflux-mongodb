package com.test.webflux.trx.mongodb.entity.constants.statics;

public class CollectionName {

  public static final String USER = "user";
  public static final String USER_CONTACT = "user_contact";
  public static final String USER_ROLE = "user_role";

  private CollectionName() {
  }

}
