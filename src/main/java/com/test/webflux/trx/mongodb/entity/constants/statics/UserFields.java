package com.test.webflux.trx.mongodb.entity.constants.statics;

public class UserFields {

  public static final String NAME = "name";

  private UserFields() {
  }

}
