package com.test.webflux.trx.mongodb.service;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.dao.UserContact;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public interface UserContactService {

  Mono<UserContact> create(MandatoryRequest mandatoryRequest, UserContact userContact,
      boolean isCreateCache);

}
