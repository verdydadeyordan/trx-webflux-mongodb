package com.test.webflux.trx.mongodb.service;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public interface AccountService {

  Mono<Boolean> successWithoutTrx(MandatoryRequest mandatoryRequest, boolean usingCache);

  Mono<Boolean> successWithTrx(MandatoryRequest mandatoryRequest, boolean usingCache);

  Mono<Boolean> errorWithoutTrx(MandatoryRequest mandatoryRequest, boolean usingCache);

  Mono<Boolean> errorWithTrx(MandatoryRequest mandatoryRequest, boolean usingCache);
}
