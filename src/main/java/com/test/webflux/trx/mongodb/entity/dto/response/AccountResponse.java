package com.test.webflux.trx.mongodb.entity.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountResponse {

  private String id;
  private String hashId;
  private String username;
  private String name;
  private String email;
  private String phone;
  private List<UserRoleResponse> roles;

}
