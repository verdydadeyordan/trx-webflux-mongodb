package com.test.webflux.trx.mongodb.entity.common;

import com.test.webflux.trx.mongodb.entity.constants.statics.BaseMongoFields;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseMongo implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  private String id;

  @Field(BaseMongoFields.HASH_ID)
  private String hashId;

  @Version
  @Field(BaseMongoFields.VERSION)
  private Long version;

  @CreatedDate
  @Field(BaseMongoFields.CREATED_DATE)
  private Date createdDate;

  @CreatedBy
  @Field(BaseMongoFields.CREATED_BY)
  private String createdBy;

  @LastModifiedDate
  @Field(BaseMongoFields.UPDATED_DATE)
  private Date updatedDate;

  @LastModifiedBy
  @Field(BaseMongoFields.UPDATED_BY)
  private String updatedBy;

  @Field(BaseMongoFields.IS_DELETED)
  private Integer isDeleted = 0;

}
