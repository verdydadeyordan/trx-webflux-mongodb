package com.test.webflux.trx.mongodb.entity.constants.enums;

public enum SystemParameterType {
  NUMBER,
  STRING,
  BOOLEAN,
  ARRAY,
  JSON
}