package com.test.webflux.trx.mongodb.controller;

import com.test.webflux.trx.mongodb.entity.constants.statics.MandatoryRequestFields;
import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.ModelAttribute;

public class BaseController {

  @ModelAttribute
  public MandatoryRequest getMandatoryParameter(HttpServletRequest request) {
    return (MandatoryRequest) request.getAttribute(MandatoryRequestFields.MANDATORY);
  }
}
