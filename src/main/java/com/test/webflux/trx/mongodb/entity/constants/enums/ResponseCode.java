package com.test.webflux.trx.mongodb.entity.constants.enums;

public enum ResponseCode {
  SUCCESS("SUCCESS", "SUCCESS"),
  SYSTEM_ERROR("SYSTEM_ERROR", "Contact our team"),
  DATA_NOT_EXIST("DATA_NOT_EXIST", "No data exist"),
  METHOD_NOT_IMPLEMENTED_YET("METHOD_NOT_IMPLEMENTED_YET", "The method not implemented"),
  DUPLICATE_DATA("DUPLICATE_DATA", "Duplicate data"),
  RUNTIME_ERROR("RUNTIME_ERROR", "Runtime Error"),
  BIND_ERROR("BIND_ERROR", "Please fill in mandatory parameter"),
  MIME_ERROR("MIME_ERROR", "Mime Error"),
  METHOD_ARGUMENTS_NOT_VALID("METHOD_ARGUMENTS_NOT_VALID", "Method arguments not valid"),
  DATA_NOT_VALID("DATA_NOT_VALID", "Data not valid"),
  PARSE_REQUEST_ERROR("PARSE_REQUEST_ERROR", "Please check your request payload"),
  HTTP_ERROR("HTTP_ERROR",
      "Sorry, we are not able to process your request. Please try again later.");

  private String code;
  private String message;

  ResponseCode(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}