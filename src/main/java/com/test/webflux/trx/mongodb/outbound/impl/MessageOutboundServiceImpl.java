package com.test.webflux.trx.mongodb.outbound.impl;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.constants.enums.ResponseCode;
import com.test.webflux.trx.mongodb.entity.constants.exceptions.BusinessLogicException;
import com.test.webflux.trx.mongodb.outbound.MessageOutboundService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class MessageOutboundServiceImpl implements MessageOutboundService {

  @Override
  public Mono<Boolean> sendMessage(MandatoryRequest mandatoryRequest, boolean isSuccess) {
    return Mono.just(isSuccess)
        .filter(Boolean::booleanValue)
        .switchIfEmpty(Mono.defer(
            () -> Mono.error(new BusinessLogicException(ResponseCode.METHOD_NOT_IMPLEMENTED_YET))));
  }
}
