package com.test.webflux.trx.mongodb.outbound;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import reactor.core.publisher.Mono;

public interface MessageOutboundService {

  Mono<Boolean> sendMessage(MandatoryRequest mandatoryRequest, boolean isSuccess);

}
