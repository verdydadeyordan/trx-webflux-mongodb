package com.test.webflux.trx.mongodb.utils.helper;

import com.test.webflux.trx.mongodb.entity.constants.statics.CollectionName;

public class CacheKeyHelper {

  private static final String CACHE_KEY = "com.test.webflux.trx.mongodb.";
  private static final String DOT = ".";

  private CacheKeyHelper() {
  }

  public static String createUserCacheKey(String username) {
    return CACHE_KEY + CollectionName.USER + DOT + username;
  }

  public static String createUserCacheKey(String id, String hashId) {
    return CACHE_KEY + CollectionName.USER + DOT + id + DOT + hashId;
  }

  public static String createUserContactCacheKey(String userId, String userHashId) {
    return CACHE_KEY + CollectionName.USER_CONTACT + DOT + userId + DOT + userHashId;
  }

  public static String createUserRoleCacheKey(String userId, String userHashId) {
    return CACHE_KEY + CollectionName.USER_ROLE + DOT + userId + DOT + userHashId;
  }
}
