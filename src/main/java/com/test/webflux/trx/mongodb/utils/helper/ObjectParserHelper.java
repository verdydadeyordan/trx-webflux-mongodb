package com.test.webflux.trx.mongodb.utils.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ObjectParserHelper {

  private ObjectParserHelper() {
  }

  public static <T> T objectParser(ObjectMapper mapper, String value, Class<T> clazz) {
    try {
      return mapper.readValue(value, clazz);
    } catch (IOException throwable) {
      log.error("Error on parse Object. value = {}, error = {}", value, throwable);
      return null;
    }
  }

  public static String objectWriter(ObjectMapper mapper, Object value) {
    try {
      return mapper.writeValueAsString(value);
    } catch (JsonProcessingException e) {
      log.error("Error on write object.", e);
      return "";
    }
  }

}
