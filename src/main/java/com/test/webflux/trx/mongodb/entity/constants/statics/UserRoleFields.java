package com.test.webflux.trx.mongodb.entity.constants.statics;

public class UserRoleFields {

  public static final String USER_ID = "userId";
  public static final String USER_HASH_ID = "userHashId";
  public static final String ROLE_NAME = "roleName";

  private UserRoleFields() {
  }

}
