package com.test.webflux.trx.mongodb.utils.helper;

import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.DigestUtils;

@Slf4j
public class HashHelper {

  private HashHelper() {
  }

  public static String constructHashId() {
    return DigestUtils.md5DigestAsHex(UUID.randomUUID().toString().getBytes());
  }
}
