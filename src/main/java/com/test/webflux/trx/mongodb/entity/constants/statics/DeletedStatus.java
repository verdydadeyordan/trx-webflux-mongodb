package com.test.webflux.trx.mongodb.entity.constants.statics;

public class DeletedStatus {

  public static final int ACTIVE = 0;
  public static final int INACTIVE = 1;

  private DeletedStatus() {
  }

}
