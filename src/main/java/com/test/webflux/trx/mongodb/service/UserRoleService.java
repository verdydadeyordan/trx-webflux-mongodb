package com.test.webflux.trx.mongodb.service;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.dao.UserRole;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public interface UserRoleService {

  Mono<UserRole> create(MandatoryRequest mandatoryRequest, UserRole userRole,
      boolean isCreateCache);
}
