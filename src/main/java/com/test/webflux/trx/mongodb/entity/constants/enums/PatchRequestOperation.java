package com.test.webflux.trx.mongodb.entity.constants.enums;

public enum PatchRequestOperation {
  add("add"),
  copy("copy"),
  move("move"),
  remove("remove"),
  replace("replace"),
  test("test");

  public String code;

  PatchRequestOperation(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }
}
