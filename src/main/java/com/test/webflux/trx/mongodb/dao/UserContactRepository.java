package com.test.webflux.trx.mongodb.dao;

import com.test.webflux.trx.mongodb.entity.dao.UserContact;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserContactRepository extends
    ReactiveMongoRepository<UserContact, String> {

  Mono<UserContact> findByUserIdAndUserHashIdAndIsDeleted(String userId, String userHashId,
      int isDeleted);

}
