package com.test.webflux.trx.mongodb.entity.dao;

import com.test.webflux.trx.mongodb.entity.common.BaseMongo;
import com.test.webflux.trx.mongodb.entity.constants.statics.CollectionName;
import com.test.webflux.trx.mongodb.entity.constants.statics.UserContactFields;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@EqualsAndHashCode
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = CollectionName.USER_CONTACT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserContact extends BaseMongo {

  @Field(UserContactFields.USER_ID)
  private String userId;

  @Field(UserContactFields.USER_HASH_ID)
  private String userHashId;

  @Field(UserContactFields.EMAIL)
  private String email;

  @Field(UserContactFields.PHONE)
  private String phone;

}
