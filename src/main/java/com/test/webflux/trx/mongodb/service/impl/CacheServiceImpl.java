package com.test.webflux.trx.mongodb.service.impl;

import com.test.webflux.trx.mongodb.service.CacheService;
import com.test.webflux.trx.mongodb.utils.helper.ObjectParserHelper;
import com.test.webflux.trx.mongodb.utils.helper.RetryHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Duration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class CacheServiceImpl implements CacheService {

  @Autowired
  private ReactiveRedisOperations<String, String> operations;

  @Autowired
  private ObjectMapper mapper;

  @Override
  public <T> Mono<T> findCacheByKey(String key, Class<T> clazz) {
    return operations
        .opsForValue()
        .get(key)
        .retryWhen(RetryHelper.retrySpec())
        .map(value -> ObjectParserHelper.objectParser(mapper, value, clazz))
        .onErrorResume(e -> {
          log.error("Error on findCacheByKey. key = {}, error = {}", key, e);
          return Mono.empty();
        });
  }

  @Override
  public <T> Mono<Boolean> createCache(String key, T value) {
    return Mono.just(value)
        .map(t -> ObjectParserHelper.objectWriter(mapper, value))
        .filter(s -> !s.isEmpty())
        .flatMap(s -> operations.opsForValue().set(key, s)
            .retryWhen(RetryHelper.retrySpec()))
        .switchIfEmpty(Mono.just(false))
        .onErrorResume(e -> {
          log.error("Error on createCache. key = {}, error = {}", key, e);
          return Mono.just(false);
        });
  }

  @Override
  public <T> Mono<Boolean> createCache(String key, T value, long ttl) {
    return Mono.just(value)
        .map(t -> ObjectParserHelper.objectWriter(mapper, value))
        .flatMap(s -> operations.opsForValue().set(key, s, Duration.ofSeconds(ttl))
            .retryWhen(RetryHelper.retrySpec()))
        .switchIfEmpty(Mono.just(false))
        .onErrorResume(e -> {
          log.error("Error on createCache. key = {}, error = {}", key, e);
          return Mono.just(false);
        });
  }

  @Override
  public Mono<Long> deleteCache(String key) {
    return operations.delete(key)
        .retryWhen(RetryHelper.retrySpec());
  }

}
