package com.test.webflux.trx.mongodb.service.impl;

import com.test.webflux.trx.mongodb.dao.UserContactRepository;
import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.constants.enums.ResponseCode;
import com.test.webflux.trx.mongodb.entity.constants.exceptions.BusinessLogicException;
import com.test.webflux.trx.mongodb.entity.constants.statics.DeletedStatus;
import com.test.webflux.trx.mongodb.entity.dao.UserContact;
import com.test.webflux.trx.mongodb.service.CacheService;
import com.test.webflux.trx.mongodb.service.UserContactService;
import com.test.webflux.trx.mongodb.utils.helper.CacheKeyHelper;
import com.test.webflux.trx.mongodb.utils.helper.HashHelper;
import com.test.webflux.trx.mongodb.utils.helper.RetryHelper;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UserContactServiceImpl implements UserContactService {

  @Autowired
  private CacheService cacheService;

  @Autowired
  private UserContactRepository userContactRepository;

  @Override
  public Mono<UserContact> create(MandatoryRequest mandatoryRequest, UserContact user,
      boolean isCreateCache) {
    return this.findByUserIdAndUserHashId(user.getUserId(), user.getUserHashId())
        .doOnNext(existing -> {
          throw new BusinessLogicException(ResponseCode.DUPLICATE_DATA);
        })
        .switchIfEmpty(Mono.defer(() -> Mono.just(user)
            .doOnNext(s -> {
              s.setIsDeleted(DeletedStatus.ACTIVE);
              s.setCreatedDate(new Date());
              s.setCreatedBy(mandatoryRequest.getUserId());
              s.setHashId(HashHelper.constructHashId());
            })
            .flatMap(u -> this.save(u, isCreateCache))));
  }

  private Mono<UserContact> findByUserIdAndUserHashId(String userId, String userHashId) {
    return cacheService
        .findCacheByKey(CacheKeyHelper.createUserContactCacheKey(userId, userHashId),
            UserContact.class)
        .switchIfEmpty(Mono.defer(() -> userContactRepository
            .findByUserIdAndUserHashIdAndIsDeleted(userId, userHashId, DeletedStatus.ACTIVE)));
  }

  private Mono<UserContact> save(UserContact user, boolean isCreateCache) {
    return userContactRepository.save(user)
        .retryWhen(RetryHelper.retrySpec())
        .flatMap(u -> {
          if (isCreateCache) {
            return this.createCache(u);
          }
          return Mono.just(u);
        });
  }

  private Mono<UserContact> createCache(UserContact userContact) {
    return cacheService.createCache(
        CacheKeyHelper.createUserContactCacheKey(userContact.getId(), userContact.getHashId()),
        userContact)
        .flatMap(a -> cacheService.createCache(
            CacheKeyHelper.createUserContactCacheKey(userContact.getId(), userContact.getHashId()),
            userContact))
        .thenReturn(userContact);
  }
}
