package com.test.webflux.trx.mongodb.utils.helper;

import java.util.Optional;
import org.springframework.web.context.request.WebRequest;

public class ErrorHandlerHelper {

  public ErrorHandlerHelper() {
  }

  public static String getRequestId(WebRequest webRequest) {
    return Optional.ofNullable(webRequest.getHeader("requestId")).orElse("");
  }

  public static String concatMessageWithRequestId(String message, String requestId) {
    return "[" + requestId + "] " + message;
  }
}
