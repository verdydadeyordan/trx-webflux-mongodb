package com.test.webflux.trx.mongodb.config;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.constants.statics.MandatoryRequestFields;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class InterceptorRequest extends HandlerInterceptorAdapter {

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler) {
    MandatoryRequest mandatoryRequest = this.buildMandatoryRequest(request);
    request.setAttribute(MandatoryRequestFields.MANDATORY, mandatoryRequest);
    return true;
  }

  private MandatoryRequest buildMandatoryRequest(HttpServletRequest request) {
    return MandatoryRequest.builder()
        .userId(request.getHeader(MandatoryRequestFields.USER_ID))
        .requestId(request.getHeader(MandatoryRequestFields.REQUEST_ID))
        .serviceId(request.getHeader(MandatoryRequestFields.SERVICE_ID))
        .build();
  }
}