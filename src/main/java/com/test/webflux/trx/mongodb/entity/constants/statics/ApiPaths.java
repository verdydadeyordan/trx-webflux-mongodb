package com.test.webflux.trx.mongodb.entity.constants.statics;

public class ApiPaths {

  private static final String BASE_PATH = "/trx-webflux-mongodb";

  public static final String ACCOUNT = BASE_PATH + "/account";

  private ApiPaths() {
  }

}
