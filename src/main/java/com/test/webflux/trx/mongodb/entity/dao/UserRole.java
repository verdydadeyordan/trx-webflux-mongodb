package com.test.webflux.trx.mongodb.entity.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.test.webflux.trx.mongodb.entity.common.BaseMongo;
import com.test.webflux.trx.mongodb.entity.constants.statics.CollectionName;
import com.test.webflux.trx.mongodb.entity.constants.statics.UserRoleFields;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@EqualsAndHashCode
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = CollectionName.USER_ROLE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRole extends BaseMongo {

  @Field(UserRoleFields.USER_ID)
  private String userId;

  @Field(UserRoleFields.USER_HASH_ID)
  private String userHashId;

  @Field(UserRoleFields.ROLE_NAME)
  private String roleName;

}
