package com.test.webflux.trx.mongodb.service;

import reactor.core.publisher.Mono;

public interface CacheService {

  <T> Mono<T> findCacheByKey(String key, Class<T> clazz);

  <T> Mono<Boolean> createCache(String key, T value);

  <T> Mono<Boolean> createCache(String key, T value, long expirySeconds);

  Mono<Long> deleteCache(String key);

}