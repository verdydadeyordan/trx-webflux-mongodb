package com.test.webflux.trx.mongodb.utils.helper;

import com.test.webflux.trx.mongodb.entity.constants.enums.ResponseCode;
import com.test.webflux.trx.mongodb.entity.constants.exceptions.BusinessLogicException;
import java.time.Duration;
import lombok.extern.slf4j.Slf4j;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

@Slf4j
public class RetryHelper {

  private static final int MAX_ATTEMPTS = 3;

  private RetryHelper() {
  }

  public static Retry retrySpec() {
    return Retry.backoff(MAX_ATTEMPTS, Duration.ofSeconds(1L))
        .doAfterRetry(retrySignal -> log.info("Retried {}, {}", retrySignal.totalRetries(),
            retrySignal.failure().getMessage()))
        .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) ->
            new BusinessLogicException(ResponseCode.HTTP_ERROR))
        .scheduler(Schedulers.elastic());
  }
}
