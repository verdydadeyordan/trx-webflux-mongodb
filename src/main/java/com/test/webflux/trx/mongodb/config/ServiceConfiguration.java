package com.test.webflux.trx.mongodb.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;

@Configuration
@ComponentScan(basePackages = {
    "com.test.webflux.trx.mongodb.dao",
    "com.test.webflux.trx.mongodb.service",
})
@EnableConfigurationProperties({
})
@EnableReactiveMongoRepositories(basePackages = "com.test.webflux.trx.mongodb.dao")
@EnableCaching
public class ServiceConfiguration {

  @Bean
  public ReactiveRedisOperations<String, String> reactiveRedisOperations(
      ReactiveRedisConnectionFactory factory) {
    return new ReactiveStringRedisTemplate(factory);
  }

}
