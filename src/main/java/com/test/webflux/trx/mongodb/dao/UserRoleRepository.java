package com.test.webflux.trx.mongodb.dao;

import com.test.webflux.trx.mongodb.entity.dao.UserRole;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRoleRepository extends
    ReactiveMongoRepository<UserRole, String> {

  Mono<UserRole> findByUserIdAndUserHashIdAndRoleNameAndIsDeleted(String userId, String userHashId,
      String roleName, int isDeleted);

}
