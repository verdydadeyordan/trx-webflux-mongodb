package com.test.webflux.trx.mongodb.entity.constants.enums;

public enum RoleMenuAccess {
  READ,
  CREATE,
  UPDATE,
  DELETE
}