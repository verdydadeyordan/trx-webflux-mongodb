package com.test.webflux.trx.mongodb.config;

import static springfox.documentation.builders.PathSelectors.regex;

import com.test.webflux.trx.mongodb.entity.constants.statics.MandatoryRequestFields;
import java.util.Arrays;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

  private static final String STRING = "string";
  private static final String HEADER = "header";

  @Bean
  public Docket init() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.test.webflux.trx.mongodb.controller"))
        .paths(regex("/.*"))
        .build()
        .globalOperationParameters(Arrays.asList(
            new ParameterBuilder().name(MandatoryRequestFields.REQUEST_ID).parameterType(HEADER)
                .modelRef(new ModelRef(STRING)).required(true).defaultValue("123456789")
                .description("client's request id").build(),
            new ParameterBuilder().name(MandatoryRequestFields.SERVICE_ID).parameterType(HEADER)
                .modelRef(new ModelRef(STRING)).required(true).defaultValue("service-id")
                .description("client's service id").build(),
            new ParameterBuilder().name(MandatoryRequestFields.USER_ID).parameterType(HEADER)
                .modelRef(new ModelRef(STRING)).required(true)
                .defaultValue("5ef9ef82f8f12c04d61edf31")
                .description("client's access id").build()))
        .genericModelSubstitutes(DeferredResult.class, ResponseEntity.class);
  }
}