package com.test.webflux.trx.mongodb.entity.constants.statics;

public class UserContactFields {

  public static final String USER_ID = "userId";
  public static final String USER_HASH_ID = "userHashId";
  public static final String EMAIL = "email";
  public static final String PHONE = "phone";

  private UserContactFields() {
  }

}
