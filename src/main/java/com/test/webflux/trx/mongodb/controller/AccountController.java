package com.test.webflux.trx.mongodb.controller;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.constants.statics.ApiPaths;
import com.test.webflux.trx.mongodb.entity.dto.common.BaseResponse;
import com.test.webflux.trx.mongodb.service.AccountService;
import com.test.webflux.trx.mongodb.utils.helper.ResponseHelper;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@RestController
@RequestMapping(ApiPaths.ACCOUNT)
public class AccountController extends BaseController {

  @Autowired
  private AccountService accountService;

  @PostMapping("/successWithoutTrx")
  public Mono<BaseResponse<Boolean>> successWithoutTrx(
      @ApiIgnore @Valid @ModelAttribute MandatoryRequest mandatoryRequest,
      @RequestParam boolean usingCache) {
    return accountService
        .successWithoutTrx(mandatoryRequest, usingCache)
        .map(ResponseHelper::constructSuccessResponse);
  }

  @PostMapping("/successWithTrx")
  public Mono<BaseResponse<Boolean>> successWithTrx(
      @ApiIgnore @Valid @ModelAttribute MandatoryRequest mandatoryRequest,
      @RequestParam boolean usingCache) {
    return accountService
        .successWithTrx(mandatoryRequest, usingCache)
        .map(ResponseHelper::constructSuccessResponse);
  }

  @PostMapping("/errorWithoutTrx")
  public Mono<BaseResponse<Boolean>> errorWithoutTrx(
      @ApiIgnore @Valid @ModelAttribute MandatoryRequest mandatoryRequest,
      @RequestParam boolean usingCache) {
    return accountService
        .errorWithoutTrx(mandatoryRequest, usingCache)
        .map(ResponseHelper::constructSuccessResponse);
  }

  @PostMapping("/errorWithTrx")
  public Mono<BaseResponse<Boolean>> errorWithTrx(
      @ApiIgnore @Valid @ModelAttribute MandatoryRequest mandatoryRequest,
      @RequestParam boolean usingCache) {
    return accountService
        .errorWithTrx(mandatoryRequest, usingCache)
        .map(ResponseHelper::constructSuccessResponse);
  }
}
