package com.test.webflux.trx.mongodb.entity.constants.statics;

public class MandatoryRequestFields {

  public static final String MANDATORY = "mandatory";
  public static final String USER_ID = "x-access-id";
  public static final String REQUEST_ID = "x-request-id";
  public static final String SERVICE_ID = "x-service-id";

  private MandatoryRequestFields() {
  }
}
