package com.test.webflux.trx.mongodb.utils.helper;

import com.test.webflux.trx.mongodb.entity.constants.enums.ResponseCode;
import com.test.webflux.trx.mongodb.entity.dto.common.BaseResponse;
import java.util.Date;
import java.util.List;

public class ResponseHelper {

  private ResponseHelper() {
  }

  @SuppressWarnings("unchecked")
  public static <T> BaseResponse<T> constructSuccessResponse(T data) {
    return BaseResponse.<T>builder()
        .code(ResponseCode.SUCCESS.getCode())
        .message(ResponseCode.SUCCESS.getMessage())
        .serverTime(new Date())
        .data(data)
        .build();
  }

  public static BaseResponse constructErrorResponse(
      String code, String message, List<String> errors) {
    return BaseResponse.builder()
        .code(code)
        .message(message)
        .serverTime(new Date())
        .errors(errors)
        .build();
  }
}
