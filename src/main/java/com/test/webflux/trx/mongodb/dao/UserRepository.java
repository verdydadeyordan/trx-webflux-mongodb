package com.test.webflux.trx.mongodb.dao;

import com.test.webflux.trx.mongodb.entity.dao.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends
    ReactiveMongoRepository<User, String> {

  Mono<User> findByIdAndHashIdAndIsDeleted(String id, String hashId, int isDeleted);

  Mono<User> findByNameAndIsDeleted(String name, int isDeleted);

}
