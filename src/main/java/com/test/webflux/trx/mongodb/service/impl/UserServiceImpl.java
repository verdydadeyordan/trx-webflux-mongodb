package com.test.webflux.trx.mongodb.service.impl;

import com.test.webflux.trx.mongodb.dao.UserRepository;
import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.constants.enums.ResponseCode;
import com.test.webflux.trx.mongodb.entity.constants.exceptions.BusinessLogicException;
import com.test.webflux.trx.mongodb.entity.constants.statics.DeletedStatus;
import com.test.webflux.trx.mongodb.entity.dao.User;
import com.test.webflux.trx.mongodb.service.CacheService;
import com.test.webflux.trx.mongodb.service.UserService;
import com.test.webflux.trx.mongodb.utils.helper.CacheKeyHelper;
import com.test.webflux.trx.mongodb.utils.helper.HashHelper;
import com.test.webflux.trx.mongodb.utils.helper.RetryHelper;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private CacheService cacheService;

  @Autowired
  private UserRepository userRepository;

  @Override
  public Mono<User> create(MandatoryRequest mandatoryRequest, User user, boolean isCreateCache) {
    return this.findByName(user.getName())
        .doOnNext(existing -> {
          throw new BusinessLogicException(ResponseCode.DUPLICATE_DATA);
        })
        .switchIfEmpty(Mono.defer(() -> Mono.just(user)
            .doOnNext(s -> {
              s.setIsDeleted(DeletedStatus.ACTIVE);
              s.setCreatedDate(new Date());
              s.setCreatedBy(mandatoryRequest.getUserId());
              s.setHashId(HashHelper.constructHashId());
            })
            .flatMap(u -> this.save(u, isCreateCache))));
  }

  private Mono<User> findByName(String username) {
    return cacheService
        .findCacheByKey(CacheKeyHelper.createUserCacheKey(username), User.class)
        .switchIfEmpty(Mono.defer(() -> userRepository
            .findByNameAndIsDeleted(username, DeletedStatus.ACTIVE)));
  }

  private Mono<User> save(User user, boolean isCreateCache) {
    return userRepository.save(user)
        .retryWhen(RetryHelper.retrySpec())
        .flatMap(u -> {
          if (isCreateCache) {
            return this.createCache(u);
          }
          return Mono.just(u);
        });
  }

  private Mono<User> createCache(User user) {
    return cacheService.createCache(
        CacheKeyHelper.createUserCacheKey(user.getId(), user.getHashId()), user)
        .flatMap(a -> cacheService.createCache(
            CacheKeyHelper.createUserCacheKey(user.getName()), user))
        .thenReturn(user);
  }
}
