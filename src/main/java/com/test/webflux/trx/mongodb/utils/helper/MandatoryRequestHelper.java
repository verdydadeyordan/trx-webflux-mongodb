package com.test.webflux.trx.mongodb.utils.helper;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.constants.statics.MandatoryRequestFields;
import java.util.HashMap;
import java.util.Map;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class MandatoryRequestHelper {

  private MandatoryRequestHelper() {
  }

  public static Map<String, Object> toHeaders(MandatoryRequest mandatoryRequest) {
    Map<String, Object> headers = new HashMap<>();
    headers.put(MandatoryRequestFields.REQUEST_ID, mandatoryRequest.getRequestId());
    return headers;
  }

  public static MultiValueMap<String, String> getHeaderMap(MandatoryRequest mandatoryRequest) {
    MultiValueMap<String, String> headerMap = new LinkedMultiValueMap<>();
    headerMap.set(MandatoryRequestFields.REQUEST_ID, mandatoryRequest.getRequestId());
    headerMap.set(MandatoryRequestFields.SERVICE_ID, mandatoryRequest.getServiceId());
    headerMap.set(MandatoryRequestFields.USER_ID, mandatoryRequest.getUserId());
    return headerMap;
  }

}
