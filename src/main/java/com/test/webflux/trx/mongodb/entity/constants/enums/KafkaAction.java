package com.test.webflux.trx.mongodb.entity.constants.enums;

public enum KafkaAction {

  CREATE("CREATE"),
  UPDATE("UPDATE"),
  DELETE("DELETE");

  private String code;

  KafkaAction(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

}