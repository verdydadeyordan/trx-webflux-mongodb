package com.test.webflux.trx.mongodb.service.impl;

import com.test.webflux.trx.mongodb.dao.UserRoleRepository;
import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.constants.enums.ResponseCode;
import com.test.webflux.trx.mongodb.entity.constants.exceptions.BusinessLogicException;
import com.test.webflux.trx.mongodb.entity.constants.statics.DeletedStatus;
import com.test.webflux.trx.mongodb.entity.dao.UserRole;
import com.test.webflux.trx.mongodb.service.CacheService;
import com.test.webflux.trx.mongodb.service.UserRoleService;
import com.test.webflux.trx.mongodb.utils.helper.CacheKeyHelper;
import com.test.webflux.trx.mongodb.utils.helper.HashHelper;
import com.test.webflux.trx.mongodb.utils.helper.RetryHelper;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UserRoleServiceImpl implements UserRoleService {

  @Autowired
  private CacheService cacheService;

  @Autowired
  private UserRoleRepository userRoleRepository;

  @Override
  public Mono<UserRole> create(MandatoryRequest mandatoryRequest, UserRole user,
      boolean isCreateCache) {
    return this.findByUserIdAndUserHashIdAndRoleName(user.getUserId(), user.getUserHashId(),
        user.getRoleName())
        .doOnNext(existing -> {
          throw new BusinessLogicException(ResponseCode.DUPLICATE_DATA);
        })
        .switchIfEmpty(Mono.defer(() -> Mono.just(user)
            .doOnNext(s -> {
              s.setIsDeleted(DeletedStatus.ACTIVE);
              s.setCreatedDate(new Date());
              s.setCreatedBy(mandatoryRequest.getUserId());
              s.setHashId(HashHelper.constructHashId());
            })
            .flatMap(u -> this.save(u, isCreateCache))));
  }

  private Mono<UserRole> findByUserIdAndUserHashIdAndRoleName(String userId, String userHashId,
      String rolelName) {
    return cacheService
        .findCacheByKey(CacheKeyHelper.createUserRoleCacheKey(userId, userHashId), UserRole.class)
        .switchIfEmpty(Mono.defer(() -> userRoleRepository
            .findByUserIdAndUserHashIdAndRoleNameAndIsDeleted(userId, userHashId, rolelName,
                DeletedStatus.ACTIVE)));
  }

  private Mono<UserRole> save(UserRole user, boolean isCreateCache) {
    return userRoleRepository.save(user)
        .retryWhen(RetryHelper.retrySpec())
        .flatMap(u -> {
          if (isCreateCache) {
            return this.createCache(u);
          }
          return Mono.just(u);
        });
  }

  private Mono<UserRole> createCache(UserRole userRole) {
    return cacheService.createCache(
        CacheKeyHelper.createUserRoleCacheKey(userRole.getId(), userRole.getHashId()), userRole)
        .flatMap(a -> cacheService.createCache(
            CacheKeyHelper.createUserRoleCacheKey(userRole.getId(), userRole.getHashId()),
            userRole))
        .thenReturn(userRole);
  }
}
