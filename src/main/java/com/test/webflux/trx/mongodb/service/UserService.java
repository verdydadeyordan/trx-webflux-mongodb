package com.test.webflux.trx.mongodb.service;

import com.test.webflux.trx.mongodb.entity.common.MandatoryRequest;
import com.test.webflux.trx.mongodb.entity.dao.User;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public interface UserService {

  Mono<User> create(MandatoryRequest mandatoryRequest, User user, boolean isCreateCache);
}
