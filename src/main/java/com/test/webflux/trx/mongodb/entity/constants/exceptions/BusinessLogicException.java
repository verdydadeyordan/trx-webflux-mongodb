package com.test.webflux.trx.mongodb.entity.constants.exceptions;

import com.test.webflux.trx.mongodb.entity.constants.enums.ResponseCode;
import lombok.Data;

@Data
public class BusinessLogicException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private String code;
  private String message;

  public BusinessLogicException(String code, String message) {
    super();
    this.setCode(code);
    this.setMessage(message);
  }

  public BusinessLogicException(ResponseCode responseCode) {
    super();
    this.setCode(responseCode.getCode());
    this.setMessage(responseCode.getMessage());
  }
}
