package com.test.webflux.trx.mongodb.controller;

import com.test.webflux.trx.mongodb.entity.constants.enums.ResponseCode;
import com.test.webflux.trx.mongodb.entity.constants.exceptions.BusinessLogicException;
import com.test.webflux.trx.mongodb.utils.helper.ErrorHandlerHelper;
import com.test.webflux.trx.mongodb.utils.helper.ResponseHelper;
import com.test.webflux.trx.mongodb.entity.dto.common.BaseResponse;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestControllerAdvice
public class ErrorHandlerController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandlerController.class);

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse methodArgumentNotValidException(
      MethodArgumentNotValidException manve, WebRequest webRequest) {
    this.logError(webRequest, "MethodArgumentNotValidException.", manve, LOGGER);
    List<FieldError> methodArgumentNotValidExceptionErrors =
        manve.getBindingResult().getFieldErrors();
    List<String> errors = methodArgumentNotValidExceptionErrors.stream().map(fieldError ->
        fieldError.getField() + " " + fieldError.getDefaultMessage()
    ).collect(Collectors.toList());
    return ResponseHelper.constructErrorResponse(
        ResponseCode.METHOD_ARGUMENTS_NOT_VALID.getCode(),
        ResponseCode.METHOD_ARGUMENTS_NOT_VALID.getMessage(),
        errors);
  }

  @ExceptionHandler(BindException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse bindException(BindException be, WebRequest webRequest) {
    this.logError(webRequest, "BindException.", be, LOGGER);
    List<FieldError> bindErrors = be.getFieldErrors();
    List<String> errors = bindErrors.stream().map(fieldError ->
        fieldError.getField() + " " + fieldError.getDefaultMessage()
    ).collect(Collectors.toList());
    return ResponseHelper.constructErrorResponse(
        ResponseCode.BIND_ERROR.getCode(), ResponseCode.BIND_ERROR.getMessage(), errors);
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse httpMessageNotReadableException(
      HttpMessageNotReadableException he, WebRequest webRequest) {
    this.logError(webRequest, "HttpMessageNotReadableException.", he, LOGGER);
    return ResponseHelper.constructErrorResponse(
        ResponseCode.PARSE_REQUEST_ERROR.getCode(),
        ResponseCode.PARSE_REQUEST_ERROR.getMessage(),
        null);
  }

  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse httpMediaTypeNotSupportedException(
      HttpMediaTypeNotSupportedException me, WebRequest webRequest) {
    this.logError(webRequest, "HttpMediaTypeNotSupportedException.", me, LOGGER);
    return ResponseHelper.constructErrorResponse(
        ResponseCode.MIME_ERROR.getCode(), ResponseCode.MIME_ERROR.getMessage(), null);
  }

  @ExceptionHandler(MissingServletRequestParameterException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public BaseResponse missingServletRequestParameterException(
      MissingServletRequestParameterException me, WebRequest webRequest) {
    this.logError(webRequest, "MissingServletRequestParameterException.", me, LOGGER);
    return ResponseHelper.constructErrorResponse(
        ResponseCode.BIND_ERROR.getCode(), ResponseCode.BIND_ERROR.getMessage(), null);
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public BaseResponse exception(Exception e, WebRequest webRequest) {
    this.logError(webRequest, "Exception.", e, LOGGER);
    return ResponseHelper.constructErrorResponse(
        ResponseCode.SYSTEM_ERROR.getCode(), ResponseCode.SYSTEM_ERROR.getMessage(), null);
  }

  @ExceptionHandler(RuntimeException.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public BaseResponse runTimeException(RuntimeException re, WebRequest webRequest) {
    this.logError(webRequest, "RuntimeException.", re, LOGGER);
    return ResponseHelper.constructErrorResponse(
        ResponseCode.RUNTIME_ERROR.getCode(), ResponseCode.RUNTIME_ERROR.getMessage(), null);
  }

  @ExceptionHandler(BusinessLogicException.class)
  public BaseResponse businessLogicException(BusinessLogicException ble, WebRequest webRequest) {
    this.logError(webRequest, "BusinessLogicException.", ble, LOGGER);
    return ResponseHelper.constructErrorResponse(ble.getCode(), ble.getMessage(), null);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
  public BaseResponse illegalArgumentException(IllegalArgumentException ie, WebRequest webRequest) {
    this.logError(webRequest, "IllegalArgumentException.", ie, LOGGER);
    return ResponseHelper.constructErrorResponse(
        ResponseCode.DATA_NOT_VALID.getCode(), ResponseCode.DATA_NOT_VALID.getMessage(), null);
  }

  private void logError(WebRequest webRequest, String exceptionType, Exception exception,
      Logger log) {
    String logErrorMessage = ErrorHandlerHelper
        .concatMessageWithRequestId(exceptionType, ErrorHandlerHelper.getRequestId(webRequest));
    log.error(logErrorMessage, exception);
  }

}
