# TRX-WEBFLUX-MONGODB

Hello there... This is the sample code of how to using the MongoDB Webflux transaction.
I hope you enjoy this tutorial.

## Note
If you don't know the Transactional concept, you can read this blog
[Spring Transaction Management](https://vladmihalcea.com/spring-transactional-annotation/).
I think this blog is good enough to understand the concept. 

## How To Run The Project
Before you run the project, the first step is to make sure your MongoDB replicated.
if not, please follows these:

1. Find the folder `replicate_mongo` in these project
2. Run the `setup.sh`
3. After that, your local MongoDB will replicate into 3.
4. **Beware**, before you running these instructions, please backup your data, this process will erase all the MongoDB data.

## How To Read The Code
1. Search `ReactiveMongoConfiguration.java`, this is the sample class of Spring Webflux MongoDB transaction configuration
2. Search `AccountServiceImpl`, this is the sample class to add the annotation `@Transactional` in method level.
3. Using annotation `@Transactional` for declares the class or method will using the transactional concept. 

## How To Test The Project
After you run this project, you can use the swagger project in 

## The Target Course
The main target or this sample code is to make you know the reason of:
1. Why using or not using the Transactional
2. When to use the Transactional
3. Why using or not using the Redis Cache
4. When to use the Redis Cache
5. And when you should use the Webflux Retryable method

 
